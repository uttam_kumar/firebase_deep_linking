package com.example.uttam.deeplinkfireb;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener  {

    private static final String TAG ="MainActivity" ;
    GoogleApiClient mGoogleApiClient;
    private static final int REQUEST_INVITE = 0;
    Button inviteBtn,shareBtn,defaultInviteBtn;
    Uri deepLink = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent=getIntent();
        String action=intent.getAction();
        Uri data=intent.getData();

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            Log.d(TAG, "getDynamicLink:onSuccess"+deepLink);
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "getDynamicLink:onFailure", e);
                    }
                });


        defaultInviteBtn=findViewById(R.id.defaultInviteBtnId);
        defaultInviteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDefaultInviteClicked();
            }
        });


        inviteBtn=findViewById(R.id.inviteBtnId);
        inviteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onInviteClicked();
            }
        });

        shareBtn=findViewById(R.id.shareBtnId);
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShareAppClicked();
            }
        });

        mGoogleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(AppInvite.API)
                .build();


        boolean autoLaunchDeepLink = true;
        AppInvite.AppInviteApi.getInvitation(mGoogleApiClient, this, autoLaunchDeepLink)
        .setResultCallback(new ResultCallback<AppInviteInvitationResult>() {
            @Override
            public void onResult(@NonNull AppInviteInvitationResult appInviteInvitationResult) {
                Log.d(TAG, String.valueOf(appInviteInvitationResult.getStatus()));

                if(appInviteInvitationResult.getStatus().isSuccess()){
                    Intent i=appInviteInvitationResult.getInvitationIntent();
                    String deepLink1= AppInviteReferral.getDeepLink(i);
                    Log.d(TAG, "deepLink1: "+deepLink1);
                }else{
                    Log.d(TAG, "no deep link found");
                }
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    //custom invite
    private void onInviteClicked() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Synopi IpTalkie");
            String strShareMessage = "\nPlease try this app\n\n";
            //strShareMessage = strShareMessage + "https://play.google.com/store/apps/details?id=" + getPackageName();
            strShareMessage = strShareMessage +"https://play.google.com/store/apps/details?id=com.synopi.iptalkie";
            //Uri screenshotUri = Uri.parse("android.resource://packagename/drawable/synopi_iptalkie_logo");
            //i.setType("image/png");
            //i.putExtra(Intent.EXTRA_STREAM, screenshotUri);
            i.putExtra(Intent.EXTRA_TEXT, strShareMessage);
            startActivity(Intent.createChooser(i, "Invite via"));
        } catch(Exception e) {
            //e.toString();
        }


    }

    //default invite
    private void onDefaultInviteClicked(){
        Intent intent = new AppInviteInvitation.IntentBuilder("Invitation")
                .setMessage("Download and Install")
                .setDeepLink(Uri.parse("https://deeplinkfireb.page.link/jTpt"))
                .setCallToActionText("Invitation cta")
                .build();
        startActivityForResult(intent, REQUEST_INVITE);
    }

    //default invite's forActivityResult receive
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    Log.d(TAG, "onActivityResult: sent invitation " + id);
                }
            } else {
                Log.d(TAG, "onActivityResult: sending failed ");
            }
        }
    }

    //link sharing through social media
    //if app is installed then it opens app otherwise it opens it's website in browser
    private void onShareAppClicked(){
        Intent a = new Intent(Intent.ACTION_SEND);

        //this is to get the app link in the playstore without launching your app.
        //final String appPackageName = getApplicationContext().getPackageName();
        Uri strAppLink = Uri.parse("https://deeplinkfireb.page.link/jTpt");
        // this is the sharing part
        a.setType("text/plain");
        String shareBody = "This is an example of dynamic deep linking, if app is installed then it opens app otherwise it opens it's website in browser" +
                "\n\n\n"+strAppLink;
        String shareSub = "Share app link";
        a.putExtra(Intent.EXTRA_SUBJECT, shareSub);
        a.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(a, "Link Sharing By"));
    }
}
